# Keyphase Password Manager - Powered by [Conshaper](https://www.conshaper.com/)

#### A fully functional online Password Manager which will be always free and open source. Works on node.js using sails.js framework. Go to [Keyphase Password Manager](https://www.keyphase.org/) to use it.

#### It is a completely secured and privacy focused system. All user data are encrypted using aes-256-gcm encryption before being stored in the database. So, the data is not accessible without the user's master password.

## Features

### Full Security and Privacy
- All data is encrypted with aes-256-gcm

## Documentation

### Coming Soon
