// Menu Toggle function
$("#menu-toggle").click(function (e) {
	e.preventDefault();
	$("#wrapper").toggleClass("toggled");
});

// Remove Hash from URL
history.pushState("", document.title, window.location.pathname);

$("#add-security-question").click(function () {
	var htmlInput = '<br>';
	htmlInput += '<div class="form-group">';
	htmlInput += '<label>Security Question: (Optional)</label>';
	htmlInput += '<input type="text" class="form-control" name="security-question">';
	htmlInput += '</div>';
	htmlInput += '<div class="form-group">';
	htmlInput += '<label>Answer: (Optional)</label>';
	htmlInput += '<input type="text" class="form-control" name="security-answer">';
	htmlInput += '</div>';

	$("#security-question-container").append(htmlInput);
});

$(document).ready(function () {
	var inputs = $('.inputfile');
  $('.inputfile').change(function() {
    for (var i = 0; i < inputs.length; i++) {
      var filename = inputs[i].value;
      if (filename.lastIndexOf('\\')) {
        // For windows
        var temp = filename.substring(0, filename.lastIndexOf('\\') + 1);
        filename = filename.replace(temp, '');
      } else {
        // For *nix systems
        var temp = filename.substring(0, filename.lastIndexOf('/') + 1);
        filename = filename.replace(temp, '');
      }
      $(inputs[i]).siblings('label').html('<i class="fa fa-camera" aria-hidden="true"></i> ' + filename);
      $(inputs[i]).siblings('button').prop('disabled', false);
    }
  });
	$('.dropdown-menu').click(function (event) {
		event.stopPropagation();
	});
	$('.copy-password').click(function (event) {
		event.preventDefault();
		event.stopPropagation();
		var indexSelected = $(this).attr('id').replace('copyPassword', '');
		var copyTextarea = $("#passwordShow" + indexSelected).html();

		var textArea = document.createElement("textarea");

		// Place in top-left corner of screen regardless of scroll position.
		textArea.style.position = 'fixed';
		textArea.style.top = 0;
		textArea.style.left = 0;

		// Ensure it has a small width and height. Setting to 1px / 1em
		// doesn't work as this gives a negative w/h on some browsers.
		textArea.style.width = '2em';
		textArea.style.height = '2em';

		// We don't need padding, reducing the size if it does flash render.
		textArea.style.padding = 0;

		// Clean up any borders.
		textArea.style.border = 'none';
		textArea.style.outline = 'none';
		textArea.style.boxShadow = 'none';

		// Avoid flash of white box if rendered for any reason.
		textArea.style.background = 'transparent';


		textArea.value = copyTextarea;

		document.body.appendChild(textArea);

		textArea.select();

		try {
			var successful = document.execCommand('copy');
			var msg = successful ? 'successful' : 'unsuccessful';
			console.log('Copying text command was ' + msg);
		} catch (err) {
			console.log('Oops, unable to copy');
		}

		document.body.removeChild(textArea);
	});

	$(".site-list-expand").click(function () {
		var indexSelected = $(this).attr('id').replace('passwordExpand', '');

		$("#detailsModal .modal-title").html('<a href="http://' + $("#siteURLHidden" + indexSelected).val() + '" target="_blank" rel="nofollow">Password for ' + $("#siteNameHidden" + indexSelected).val() + '</a>');

		$("#detailsModal #modalRecordId").val($("#siteRecordId" + indexSelected).val());

		$("#detailsModal #modalEmailDetails").val($("#siteEmailHidden" + indexSelected).val());
		$("#detailsModal #modalUsernameDetails").val($("#siteUsernameHidden" + indexSelected).val());
		$("#detailsModal #modalPasswordDetails").val($("#sitePasswordHidden" + indexSelected).val());

		var securityQuestions = $(".securityQuestionHidden" + indexSelected).map(function () {
			return this.value;
		}).get();
		var securityAnswers = $(".securityAnswerHidden" + indexSelected).map(function () {
			return this.value;
		}).get();

		var securityHtml = "";

		for (var i = 0; i < securityQuestions.length; i++) {
			if (securityQuestions[i] !== "") {
				var questionNumber = i + 1;
				securityHtml += "<div class='form-group'>";
				securityHtml += "<label>Security Question " + questionNumber + "</label>";
				securityHtml += "<p class='form-control-static'>Q: " + securityQuestions[i] + "</p>";
				securityHtml += "<p class='form-control-static'>A: " + securityAnswers[i] + "</p>";
				securityHtml += "</div>";
			}
		}
		$("#modalMasterPasswordDetails").val("");
		$("#detailsModal #modalSecurityQuestionDetails").html(securityHtml);
		$("#detailsModal").modal();
	});
	$("#setMasterPasswordForm").submit(function (event) {
		if ($("#setMasterPasswordForm input[name='master-password']").val() !== $("#setMasterPasswordForm input[name='repeat-password']").val()) {
			event.preventDefault();
		}
	});
	$('[data-toggle="tooltip"]').tooltip();
});

// Scrollbar
var container = document.getElementById('page-content-wrapper');
Ps.initialize(container);
