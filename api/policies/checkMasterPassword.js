module.exports = function (req, res, next) {
	// res.cookie('phaseChange', 'Ola Signor',
	// 	{
	// 		path: '/application',
	// 		httpOnly: true,
	// 		signed: true,
	// 		secure: true,
	// 		maxAge: 10800000 // 3 Hours
	// 	});
	Passport.findOne({
		user: req.user.id
	}).exec(function (err, passport) {
		if (err) {
			res.serverError(res);
		}
		if (passport.masterPassword === null) {
			return res.redirect('/application/setMasterPassword');
		} else if (req.signedCookies.phaseChange == undefined) {
			return res.redirect('/application/enterMasterPassword');
		} else {
			return next();
		}
	});
};
