/**
 * AuthController
 *
 * @description :: Server-side logic for managing auths
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var bcrypt = require('bcrypt');
var passport = require('passport');
var request = require('request');
var nodemailer = require('nodemailer');

module.exports = {
	_config: {
		actions: false,
		shortcuts: false,
		rest: false
	},
	login: function (req, res, next) {
		passport.authenticate('local', function (err, user, info) {
			if (err) {
				return res.serverError(err);
			} else if (!user) {
				res.redirect('/login');
			}
			req.logIn(user, function (err) {
				if (err) {
					return res.serverError(err);
				} else {
					req.session.isAuthenticated = true;
					req.session.save();
					res.redirect('/application');
				}
			});
		})(req, res, next);
	},
	logout: function (req, res, next) {
		req.logout();
		req.session.isAuthenticated = false;
		req.session.save();
		res.clearCookie('phaseChange');
		res.redirect('/login');
	},
	signup: function (req, res, next) {
		var newUser = {};
		var newPassport = {
			user: null, // To be set later
			email: req.param("email"),
			password: req.param("password"),
			loginType: "passport-local"
		};
		var profileParameters = {
			user: null
		};
		User.create(newUser).exec(function (err, user) {
			profileParameters.user = user.id;
			newPassport.user = user.id;
			Passport.create(newPassport).exec(function (err, passport) {
				if (err) {
					return res.serverError(err);
				} else {
					User.update({
						id: user.id
					}, {
						passport: passport.id
					}).exec(function (err, updatedUser) {
						if (err) {
							return res.serverError(err);
						} else {
							Profile.create(profileParameters).exec(function (err, profile) {
								User.update(user.id, {
									profile: profile.id
								}).exec(function (err, updatedUserFromProfile) {
									res.redirect('/login');
								});
							});
						}
					});
				}
			});
		});
	},

	resetPassword: function (req, res, next) {
		// Captcha Verify
		request.post({
			url: 'https://www.google.com/recaptcha/api/siteverify',
			form: {
				secret: '6LesSRAUAAAAADRugcri6ZUXrUneRNpbuQ7l9n7U',
				response: req.param('g-recaptcha-response')
			}
		}, function (err, httpResponse, body) {
			if (err) {
				return res.serverError(err);
			}

			var bodyJSON = JSON.parse(body);

			if (!bodyJSON.success) {
				return res.redirect('/reset-password');
			}

			var emailReq = req.param('email');

			require('crypto').randomBytes(128, function (err, buffer) {
				var valueToEncrypt = emailReq + buffer.toString('hex');
				// console.log(valueToEncrypt);
				CryptoService.getMasterSaltLength(function (err, saltLength) {
					bcrypt.genSalt(saltLength, function (err, salt) {
						bcrypt.hash(valueToEncrypt, salt, function (err, hash) {
							if (err) {
								res.serverError(err);
							} else {
								// hash
								Passport.findOne({
									email: emailReq
								}).exec(function (err, passport) {
									if (err) {
										return res.serverError(err);
									}
									if (!passport) {
										return res.view('auth/reset', {
											success: false
										});
									}
									Passport.update({
										email: emailReq
									}, {
										passwordResetRequest: hash
									}).exec(function (err) {
										if (err) {
											res.serverError(err);
										}
										var smtpConfig = {
											host: 'smtp.zoho.com',
											port: 465,
											secure: true, // use SSL
											auth: {
												user: 'info@sharpaccount.com',
												pass: '7/8*9-48596+'
											}
										};
										var transporter = nodemailer.createTransport(smtpConfig);

										var linkHref = 'https://localhost:1337/recover-password?code='  + encodeURIComponent(hash) + '&email=' + encodeURIComponent(emailReq);

										var mailOptions = {
											from: '"KeyPhase Password Manager" <info@sharpaccount.com>',
											to: emailReq,
											subject: 'KeyPhase Password Reset Request',
											text: hash,
											html: '<a href="' + linkHref + '">Click Here to Reset Password</a>'
										};

										transporter.sendMail(mailOptions, function (err, info) {
											if (err) {
												return res.serverError(err);
											}
											return res.view('auth/reset', {
												success: true
											});
										});
									});
								});
							}
						});
					});
				});
			});
		});
	},

	recoverPassword: function (req, res, next) {
		var emailReq = req.param('email');
		var codeReq = req.param('code');

		if (!emailReq && !codeReq) {
			return res.redirect('/login');
		}

		Passport.findOne({
			email: emailReq
		}).exec(function (err, passport) {
			if (err) {
				return res.serverError(err);
			}
			if (!passport || passport.passwordResetRequest !== codeReq) {
				return res.redirect('/login');
			}
			return res.view('auth/recover', {
				email: emailReq,
				code: codeReq
			});
		});
	},

	recoverPasswordProcessing: function (req, res, next) {
		var emailReq = req.param('email');
		var codeReq = req.param('code');
		var newPasswordReq = req.param('password');

		Passport.findOne({
			email: emailReq
		}).exec(function (err, passport) {
			if (err) {
				return res.serverError(err);
			}
			if (!passport || passport.passwordResetRequest !== codeReq) {
				return res.redirect('/login');
			}
			CryptoService.getHashSaltLength(function (err, saltLength) {
					bcrypt.genSalt(saltLength, function (err, salt) {
						bcrypt.hash(newPasswordReq, salt, function (err, hash) {
							if (err) {
								return res.serverError(err);
							} else {
								Passport.update({
									email: emailReq
								},{
									password: hash,
									passwordResetRequest: null
								}).exec(function (err) {
									if (err) {
										return res.serverError(err);
									}
									return res.view('auth/recover', {
										success: true
									});
								});
							}
						});
					});
				});
		});
	},

	google: function (req, res, next) {
		passport.authenticate('google', {
			scope: ['profile', 'email']
		})(req, res, next);
	},

	googleCallback: function (req, res, next) {
		req.session.isAuthenticated = true;
		req.session.save();
		res.redirect('/application');
	},

	facebook: function (req, res, next) {
		passport.authenticate('facebook', {
			scope: ['email', 'public_profile']
		})(req, res, next);
	},

	facebookCallback: function (req, res, next) {
		req.session.isAuthenticated = true;
		req.session.save();
		res.redirect('/application');
	}
};
