/**
 * CryptoController
 *
 * @description :: Server-side logic for managing cryptoes
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var bcrypt = require('bcrypt');

module.exports = {
	addNew: function (req, res, next) {
		var receivedMasterPassword = req.signedCookies.phaseChange;

		var RawSecurityQuestion = req.param('security-question');
		var RawSecurityAnswer = req.param('security-answer');

		if (typeof (RawSecurityQuestion) == 'string' && typeof (RawSecurityAnswer) == 'string') {
			RawSecurityQuestion = [RawSecurityQuestion];
			RawSecurityAnswer = [RawSecurityAnswer];
		}

		var dataSecurityQuestion = [];
		var dataOtherInformation = [];

		_.each(RawSecurityQuestion, function (value, index) {
			var data;
			async.series([
				function (cb) {
					CryptoService.encrypt({
						textData: value,
						masterPassword: receivedMasterPassword
					}, function (err, encryptedValue) {
						if (err) {
							res.serverError(err);
						}
						data = encryptedValue;
						cb();
					});
				}
			], function (err) {
				if (err) {
					res.serverError(err);
				}
				dataSecurityQuestion[index] = {
					question: data,
					answer: null
				};
			});
		});
		_.each(RawSecurityAnswer, function (value, index) {
			var data;
			async.series([
				function (cb) {
					CryptoService.encrypt({
						textData: value,
						masterPassword: receivedMasterPassword
					}, function (err, encryptedValue) {
						if (err) {
							res.serverError(err);
						}
						data = encryptedValue;
						cb();
					});
				}
			], function (err) {
				if (err) {
					res.serverError(err);
				}
				dataSecurityQuestion[index].answer = data;
			});
		});

		var dataInformation = {
			user: req.user.id,
			siteName: null,
			siteURL: null,
			siteEmail: null,
			siteUsername: null,
			sitePassword: null,
			securityQuestions: dataSecurityQuestion
		};

		async.parallel([function (cb) {
			CryptoService.encrypt({
				textData: req.param('website-name'),
				masterPassword: receivedMasterPassword
			}, function (err, value) {
				if (err) {
					res.serverError(err);
				}
				dataInformation.siteName = value;
				cb();
			});
		}, function (cb) {
			CryptoService.encrypt({
				textData: req.param('website-url'),
				masterPassword: receivedMasterPassword
			}, function (err, value) {
				if (err) {
					res.serverError(err);
				}
				dataInformation.siteURL = value;
				cb();
			});
		}, function (cb) {
			CryptoService.encrypt({
				textData: req.param('website-email'),
				masterPassword: receivedMasterPassword
			}, function (err, value) {
				if (err) {
					res.serverError(err);
				}
				dataInformation.siteEmail = value;
				cb();
			});
		}, function (cb) {
			CryptoService.encrypt({
				textData: req.param('website-username'),
				masterPassword: receivedMasterPassword
			}, function (err, value) {
				if (err) {
					res.serverError(err);
				}
				dataInformation.siteUsername = value;
				cb();
			});
		}, function (cb) {
			CryptoService.encrypt({
				textData: req.param('website-password'),
				masterPassword: receivedMasterPassword
			}, function (err, value) {
				if (err) {
					res.serverError(err);
				}
				dataInformation.sitePassword = value;
				cb();
			});
		}], function () {
			Information.create(dataInformation).exec(function (err, information) {
				if (err) {
					res.serverError(err);
				}
				res.redirect('/application');
			});
		});
	},

	updateMasterPassword: function (req, res, next) {
		var dataToFind = {
			user: req.user.id
		};
		var dataToUpdate = {
			masterPassword: req.param('master-password')
		};
		Passport.update(dataToFind, dataToUpdate).exec(function (err, passport) {
			res.cookie('phaseChange', req.param('master-password'), {
				httpOnly: true,
				signed: true,
				secure: true,
				maxAge: 10800000 // 3 Hours
			});
			return res.redirect('/application');
		});
	},

	checkMasterPassword: function (req, res, next) {
		var dataToFind = {
			user: req.user.id
		};
		var receivedMasterPassword = req.param('master-password');

		Passport.findOne(dataToFind).exec(function (err, passport) {
			if (err) {
				return res.serverError(err);
			}
			bcrypt.compare(receivedMasterPassword, passport.masterPassword, function (err, response) {
				if (err) {
					return res.serverError(err);
				} else if (response) {
					res.cookie('phaseChange', req.param('master-password'), {
						httpOnly: true,
						signed: true,
						secure: true,
						maxAge: 10800000 // 3 Hours
					});
				}
				return res.redirect('/application');
			});
		});
	},

	editSiteRecord: function (req, res, next) {
		var dataToFind = {
			user: req.user.id
		};

		var idReq = req.param('modalRecordId');
		var emailReq = req.param('modalEmailDetails');
		var usernameReq = req.param('modalUsernameDetails');
		var passwordReq = req.param('modalPasswordDetails');
		var masterPasswordReq = req.param('modalMasterPasswordDetails');
		var deleteReq = Boolean(req.param('delete') === '');
		var editReq = Boolean(req.param('edit') === '');

		// Check Master Password
		Passport.findOne(dataToFind).exec(function (err, passport) {
			if (err) {
				return res.serverError(err);
			}
			bcrypt.compare(masterPasswordReq, passport.masterPassword, function (err, response) {
				if (err) {
					return res.serverError(err);
				} else if (response) {
						Information.findOne(idReq).exec(function (err, information) {
							if (err) {
								return res.serverError(err);
							} else if (information.user == dataToFind.user) {
								if (deleteReq) {
									Information.destroy(idReq).exec(function (err) {
										if (err) {
											return res.serverError(err);
										}
										res.redirect('/application');
									});
								} else if (editReq) {

									var dataInformation = {
										siteEmail: null,
										siteUsername: null,
										sitePassword: null
									};

									async.parallel([function (cb) {
										CryptoService.encrypt({
											textData: emailReq,
											masterPassword: masterPasswordReq
										}, function (err, value) {
											if (err) {
												res.serverError(err);
											}
											dataInformation.siteEmail = value;
											cb();
										});
									}, function (cb) {
										CryptoService.encrypt({
											textData: usernameReq,
											masterPassword: masterPasswordReq
										}, function (err, value) {
											if (err) {
												res.serverError(err);
											}
											dataInformation.siteUsername = value;
											cb();
										});
									}, function (cb) {
										CryptoService.encrypt({
											textData: passwordReq,
											masterPassword: masterPasswordReq
										}, function (err, value) {
											if (err) {
												res.serverError(err);
											}
											dataInformation.sitePassword = value;
											cb();
										});
									}], function () {
										Information.update(idReq, dataInformation).exec(function (err, information) {
											if (err) {
												res.serverError(err);
											}
											res.redirect('/application');
										});
									});
								}
							} else {
								res.redirect('/application');
							}
						});
				} else {
					res.redirect('/application');
				}
			});
		});
	}

};
