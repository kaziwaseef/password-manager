/**
 * ApplicationController
 *
 * @description :: Server-side logic for managing applications
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var bcrypt = require('bcrypt');

module.exports = {
	'index': function (req, res, next) {

		var fileUploadDirectory = require('path').resolve(sails.config.appPath, 'uploads');

		if (sails.config.environment == 'production') {
			var fileUploadDirectory = process.env.OPENSHIFT_DATA_DIR + '/uploads';
		}

		var receivedMasterPassword = req.signedCookies.phaseChange;

		var infoToGet = {
			user: req.user.id
		};

		Information.find(infoToGet).populate('securityQuestions').exec(function (err, information) {
			if (err) {
				res.serverError(err);
			}

			CryptoService.decryptJSON({
				data: information,
				masterPassword: receivedMasterPassword
			}, function (err, valueJSON) {
				if (err) {
					res.serverError(err);
				}
				Profile.findOne(req.user.profile).exec(function (err, profile) {
					if (err) {
						res.serverError(err);
					}
					if (decodeURIComponent(profile.avatar).includes(fileUploadDirectory)) {
						profile.avatar = '/application/downloadAvatar?fd=' + profile.avatar;
					}
					res.view({
						title: "Keyphase | Site List",
						pageSlug: "application/index",
						siteList: valueJSON,
						firstName: profile.firstName,
						lastName: profile.lastName,
						avatar: profile.avatar
					});
				});
			});

		});
	},
	'new': function (req, res, next) {

		var fileUploadDirectory = require('path').resolve(sails.config.appPath, 'uploads');

		if (sails.config.environment == 'production') {
			var fileUploadDirectory = process.env.OPENSHIFT_DATA_DIR + '/uploads';
		}

		Profile.findOne(req.user.profile).exec(function (err, profile) {
			if (err) {
				res.serverError(err);
			}
			if (decodeURIComponent(profile.avatar).includes(fileUploadDirectory)) {
				profile.avatar = '/application/downloadAvatar?fd=' + profile.avatar;
			}
			res.view({
				title: "Keyphase | Add New",
				pageSlug: "application/new",
				firstName: profile.firstName,
				lastName: profile.lastName,
				avatar: profile.avatar
			});
		});
	},
	profile: function (req, res, next) {

		var fileUploadDirectory = require('path').resolve(sails.config.appPath, 'uploads');

		if (sails.config.environment == 'production') {
			var fileUploadDirectory = process.env.OPENSHIFT_DATA_DIR + '/uploads';
		}

		Profile.findOne(req.user.profile).exec(function (err, profile) {
			if (err) {
				res.serverError(err);
			}
			if (decodeURIComponent(profile.avatar).includes(fileUploadDirectory)) {
				profile.avatar = '/application/downloadAvatar?fd=' + profile.avatar;
			}
			res.view({
				title: "Keyphase | Profile",
				pageSlug: "application/profile",
				firstName: profile.firstName,
				lastName: profile.lastName,
				avatar: profile.avatar
			});
		});
	},
	settings: function (req, res, next) {

		var fileUploadDirectory = require('path').resolve(sails.config.appPath, 'uploads');

		if (sails.config.environment == 'production') {
			var fileUploadDirectory = process.env.OPENSHIFT_DATA_DIR + '/uploads';
		}

		Profile.findOne(req.user.profile).exec(function (err, profile) {
			if (err) {
				res.serverError(err);
			}
			if (decodeURIComponent(profile.avatar).includes(fileUploadDirectory)) {
				profile.avatar = '/application/downloadAvatar?fd=' + profile.avatar;
			}
			res.view({
				title: "Keyphase | Settings",
				pageSlug: "application/settings",
				firstName: profile.firstName,
				lastName: profile.lastName,
				avatar: profile.avatar
			});
		});
	},
	setMasterPassword: function (req, res, next) {

		var fileUploadDirectory = require('path').resolve(sails.config.appPath, 'uploads');

		if (sails.config.environment == 'production') {
			var fileUploadDirectory = process.env.OPENSHIFT_DATA_DIR + '/uploads';
		}

		Profile.findOne(req.user.profile).exec(function (err, profile) {
			if (err) {
				res.serverError(err);
			}
			if (decodeURIComponent(profile.avatar).includes(fileUploadDirectory)) {
				profile.avatar = '/application/downloadAvatar?fd=' + profile.avatar;
			}
			res.view({
				title: "Keyphase | Set Master Password",
				pageSlug: "application/settings",
				firstName: profile.firstName,
				lastName: profile.lastName,
				avatar: profile.avatar
			});
		});
	},
	enterMasterPassword: function (req, res, next) {

		var fileUploadDirectory = require('path').resolve(sails.config.appPath, 'uploads');

		if (sails.config.environment == 'production') {
			var fileUploadDirectory = process.env.OPENSHIFT_DATA_DIR + '/uploads';
		}

		Profile.findOne(req.user.profile).exec(function (err, profile) {
			if (err) {
				res.serverError(err);
			}
			if (decodeURIComponent(profile.avatar).includes(fileUploadDirectory)) {
				profile.avatar = '/application/downloadAvatar?fd=' + profile.avatar;
			}
			res.view({
				title: "Keyphase | Enter Master Password",
				pageSlug: "application/settings",
				firstName: profile.firstName,
				lastName: profile.lastName,
				avatar: profile.avatar
			});
		});
	},
	editName: function (req, res, next) {
		Profile.update({
			user: req.user.id
		}, {
			firstName: req.param('firstName'),
			lastName: req.param('lastName')
		}).exec(function (err) {
			if (err) {
				return res.serverError(err);
			}
			return res.redirect('/application/profile');
		});
	},
	uploadAvatar: function (req, res, next) {
		var fileUploadDirectory = require('path').resolve(sails.config.appPath, 'uploads');

		if (sails.config.environment == 'production') {
			var fileUploadDirectory = process.env.OPENSHIFT_DATA_DIR + '/uploads';
		}
		// upload to build dir for immediate use
		req.file('avatar').upload({
			dirname: fileUploadDirectory
		}, function (err, uploadedFiles) {
			if (err) {
				return res.serverError(err);
			}
			// If no files were uploaded, respond with an error.
			if (uploadedFiles.length === 0) {
				return res.badRequest('No file was uploaded');
			}
			Profile.update({
				user: req.user.id
			}, {
				avatar: encodeURIComponent(uploadedFiles[0].fd)
			}).exec(function (err) {
				if (err) {
					return res.serverError(err);
				}
				return res.redirect('/application/profile');
			});
		});
	},
	downloadAvatar: function (req, res, next) {
		// Use this to download avatar files
		var fd = req.param('fd');

		var filename = fd.replace(/^.*[\\\/]/, '');

		var validFdOrNot = fd.replace('/' + filename, '');

		var fileUploadDirectory = require('path').resolve(sails.config.appPath, 'uploads');

		if (sails.config.environment == 'production') {
			var fileUploadDirectory = process.env.OPENSHIFT_DATA_DIR + '/uploads';
		}

		if (validFdOrNot !== fileUploadDirectory) {
			res.serverError();
		}

		var SkipperDisk = require('skipper-disk');
		var fileAdapter = SkipperDisk();

		// set the filename to the same file as the user uploaded
		res.set("Content-disposition", "attachment; filename='" + filename + "'");

		// Stream the file down
		fileAdapter.read(fd)
			.on('error', function (err) {
				return res.serverError(err);
			})
			.pipe(res);
	},
	changePassword: function (req, res, next) {
		var oldPasswordReq = req.param('old-password');
		var newPasswordReq = req.param('new-password');

		Passport.findOne({
			id: req.user.id
		}).exec(function (err, passport) {
			if (err) {
				return res.serverError(err);
			}
			if (passport.loginType !== 'passport-local') {
				return res.view('flashPage', {
					title: 'Failed',
					message: 'Your account is using social login, no password to change.',
					page: '/application/settings'
				});
			}
			bcrypt.compare(oldPasswordReq, passport.password, function (err, response) {
				if (err) {
					return res.serverError(err);
				} else if (!response) {
					return res.serverError();
				} else {
					CryptoService.getHashSaltLength(function (err, saltLength) {
						bcrypt.genSalt(saltLength, function (err, salt) {
							bcrypt.hash(newPasswordReq, salt, function (err, newHash) {
								if (err) {
									return res.serverError(err);
								} else {
									Passport.update({
										id: req.user.id
									}, {
										password: newHash
									}).exec(function (err) {
										if (err) {
											return res.serverError(err);
										}
										return res.view('flashPage', {
											title: 'Success',
											message: 'Changed Password Successfully',
											page: '/application/settings'
										});
									});
								}
							});
						});
					});
				}
			});
		});
	},
	changeMasterPassword: function (req, res, next) {
		var oldMasterPasswordReq = req.param('old-master-password');
		var newMasterPasswordReq = req.param('new-master-password');

		var infoToGet = {
			user: req.user.id
		};

		Passport.findOne(infoToGet).exec(function (err, passport) {
			if (err) {
				return res.serverError(err);
			}
			bcrypt.compare(oldMasterPasswordReq, passport.masterPassword, function (err, response) {
				if (err) {
					return res.serverError(err);
				} else if (!response) {
					return res.serverError();
				} else {
					Information.find(infoToGet).populate('securityQuestions').exec(function (err, information) {
						if (err) {
							return res.serverError(err);
						}
						CryptoService.decryptJSON({
							data: information,
							masterPassword: oldMasterPasswordReq
						}, function (err, valueJSON) {
							if (err) {
								return res.serverError(err);
							}
							CryptoService.encryptJSON({
								data: valueJSON,
								masterPassword: newMasterPasswordReq
							}, function (err, valueJSON) {
								if (err) {
									return res.serverError(err);
								}
								_.each(valueJSON, function (item) {
									var updateData = item;
									async.series([function (cb) {
										Information.update({
											id: item.id
										}, updateData).exec(function (err) {
											if (err) {
												return res.serverError(err);
											}
											cb();
										});
									}]);
								});
								Passport.update(infoToGet,{
										masterPassword: newMasterPasswordReq
									}).exec(function (err) {
										if (err) {
											return res.serverError(err);
										}
										res.cookie('phaseChange', newMasterPasswordReq, {
											httpOnly: true,
											signed: true,
											secure: true,
											maxAge: 10800000 // 3 Hours
										});
										return res.view('flashPage', {
											title: 'Success',
											message: 'Changed Master Password Successfully.',
											page: '/application/settings'
										});
									});
								// Information.update(infoToGet, valueJSON).exec(function (err) {
								// 	if (err) {
								// 		return res.serverError(err);
								// 	}
								// 	Passport.update(infoToGet,{
								// 		masterPassword: newMasterPasswordReq
								// 	}).exec(function (err) {
								// 		if (err) {
								// 			return res.serverError(err);
								// 		}
								// 		res.cookie('phaseChange', newMasterPasswordReq, {
								// 			httpOnly: true,
								// 			signed: true,
								// 			secure: true,
								// 			maxAge: 10800000 // 3 Hours
								// 		});
								// 		return res.view('flashPage', {
								// 			title: 'Success',
								// 			message: 'Changed Master Password Successfully.',
								// 			page: '/application/settings'
								// 		});
								// 	});
								// });
							});
						});
					});
				}
			});
		});
	}
};
