var crypto = require('crypto');

module.exports = {
	getHashSaltLength: function (cb) {
		cb(null, sails.config.passwordConfig.passWordSaltLength);
	},
	getMasterSaltLength: function (cb) {
		cb(null, sails.config.passwordConfig.masterPassWordSaltLength);
	},
	encrypt: function (options, cb) {
		var text = options.textData;
		var masterPassword = options.masterPassword;
		// random initialization vector
		var iv = crypto.randomBytes(sails.config.passwordConfig.ivLength);
		// random salt
		var salt = crypto.randomBytes(sails.config.passwordConfig.saltLength);

		// derive key: 32 byte key length - in assumption the masterkey is a cryptographic and NOT a password there is no need for
		// a large number of iterations. It may can replaced by HKDF
		var key = crypto.pbkdf2Sync(masterPassword, salt, sails.config.passwordConfig.keyIterations, sails.config.passwordConfig.keyLength, 'sha512');

		// AES 256 GCM Mode
		var cipher = crypto.createCipheriv('aes-256-gcm', key, iv);

		// encrypt the given text
		var encrypted = Buffer.concat([cipher.update(text, 'utf8'), cipher.final()]);

		// extract the auth tag
		var tag = cipher.getAuthTag();
		// generate output
		cb(null, Buffer.concat([salt, iv, tag, encrypted]).toString('base64'));
	},
	decrypt: function (options, cb) {
		var data = options.data;
		var masterPassword = options.masterPassword;
		// base64 decoding
		var bData = new Buffer(data, 'base64');

		// convert data to buffers
		var salt = bData.slice(0, sails.config.passwordConfig.saltLength);
		var iv = bData.slice(sails.config.passwordConfig.saltLength, sails.config.passwordConfig.saltLength + sails.config.passwordConfig.ivLength);
		var tag = bData.slice(sails.config.passwordConfig.saltLength + sails.config.passwordConfig.ivLength, sails.config.passwordConfig.saltLength + sails.config.passwordConfig.ivLength + 16);
		var text = bData.slice(sails.config.passwordConfig.saltLength + sails.config.passwordConfig.ivLength + 16);

		// derive key using; 32 byte key length
		var key = crypto.pbkdf2Sync(masterPassword, salt, sails.config.passwordConfig.keyIterations, sails.config.passwordConfig.keyLength, 'sha512');

		// AES 256 GCM Mode
		var decipher = crypto.createDecipheriv('aes-256-gcm', key, iv);
		decipher.setAuthTag(tag);

		// encrypt the given text
		var decrypted = decipher.update(text, 'binary', 'utf8') + decipher.final('utf8');

		cb(null, decrypted);
	},
	decryptJSON: function (options, cb) {
		var _this = this;
		var data = options.data;
		var masterPassword = options.masterPassword;

		_.each(data, function (item) {
			async.parallel([function (cb) {
				_this.decrypt({
					data: item.siteName,
					masterPassword: masterPassword
				}, function (err, decryptedData) {
					item.siteName = decryptedData;
					cb();
				});
			}, function (cb) {
				_this.decrypt({
					data: item.siteURL,
					masterPassword: masterPassword
				}, function (err, decryptedData) {
					item.siteURL = decryptedData;
					cb();
				});
			}, function (cb) {
				_this.decrypt({
					data: item.siteEmail,
					masterPassword: masterPassword
				}, function (err, decryptedData) {
					item.siteEmail = decryptedData;
					cb();
				});
			}, function (cb) {
				_this.decrypt({
					data: item.siteUsername,
					masterPassword: masterPassword
				}, function (err, decryptedData) {
					item.siteUsername = decryptedData;
					cb();
				});
			}, function (cb) {
				_this.decrypt({
					data: item.sitePassword,
					masterPassword: masterPassword
				}, function (err, decryptedData) {
					item.sitePassword = decryptedData;
					cb();
				});
			}], function () {
				_.each(item.securityQuestions, function (securityQuestion) {
					async.parallel([function (cb) {
						_this.decrypt({
							data: securityQuestion.question,
							masterPassword: masterPassword
						}, function (err, decryptedData) {
							securityQuestion.question = decryptedData;
							cb();
						});
					}, function (cb) {
						_this.decrypt({
							data: securityQuestion.answer,
							masterPassword: masterPassword
						}, function (err, decryptedData) {
							securityQuestion.answer = decryptedData;
							cb();
						});
					}]);
				});
			});
		});
		// console.log(data);
		cb(null,data);
	},
	encryptJSON: function (options, cb) {
		var _this = this;
		var data = options.data;
		var masterPassword = options.masterPassword;

		_.each(data, function (item) {
			async.parallel([function (cb) {
				_this.encrypt({
					textData: item.siteName,
					masterPassword: masterPassword
				}, function (err, encryptedData) {
					item.siteName = encryptedData;
					cb();
				});
			}, function (cb) {
				_this.encrypt({
					textData: item.siteURL,
					masterPassword: masterPassword
				}, function (err, encryptedData) {
					item.siteURL = encryptedData;
					cb();
				});
			}, function (cb) {
				_this.encrypt({
					textData: item.siteEmail,
					masterPassword: masterPassword
				}, function (err, encryptedData) {
					item.siteEmail = encryptedData;
					cb();
				});
			}, function (cb) {
				_this.encrypt({
					textData: item.siteUsername,
					masterPassword: masterPassword
				}, function (err, encryptedData) {
					item.siteUsername = encryptedData;
					cb();
				});
			}, function (cb) {
				_this.encrypt({
					textData: item.sitePassword,
					masterPassword: masterPassword
				}, function (err, encryptedData) {
					item.sitePassword = encryptedData;
					cb();
				});
			}], function () {
				_.each(item.securityQuestions, function (securityQuestion) {
					async.parallel([function (cb) {
						_this.encrypt({
							textData: securityQuestion.question,
							masterPassword: masterPassword
						}, function (err, encryptedData) {
							securityQuestion.question = encryptedData;
							cb();
						});
					}, function (cb) {
						_this.encrypt({
							textData: securityQuestion.answer,
							masterPassword: masterPassword
						}, function (err, encryptedData) {
							securityQuestion.answer = encryptedData;
							cb();
						});
					}]);
				});
			});
		});
		// console.log(data);
		cb(null,data);
	}
};
