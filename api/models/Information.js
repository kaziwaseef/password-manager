/**
 * Information.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

	attributes: {
		user: {
			model: 'user',
			via: 'information'
		},
		siteName: {
			type: "string"
		},
		siteURL: {
			type: "string"
		},
		siteEmail: {
			type: "string"
		},
		siteUsername: {
			type: "string"
		},
		sitePassword: {
			type: "string"
		},
		securityQuestions: {
			collection: 'SecurityQuestion',
			via: 'information'
		}
		// otherInformation: {
		//   collection: 'otherInformation',
		//   via: 'information'
		// }
	},
	afterDestroy: function (destroyedRecords, cb) {
		// Destroy any child whose teacher has an ID of one of the
		// deleted teacher models
		SecurityQuestion.destroy({
			information: _.pluck(destroyedRecords, 'id')
		}).exec(cb);
	}
};
