/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

	attributes: {
		passport: {
			model: 'passport',
			via: 'user'
		},
		profile: {
			model: 'profile',
			via: 'user'
		},
		information: {
			collection: 'information',
			via: 'user'
		},
		settings: {
			model: 'settings',
			via: 'user'
		},
		loginRecord: {
			collection: 'loginRecord',
			via: 'user'
		}
	}
};
