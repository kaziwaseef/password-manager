/**
 * Profile.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    user: {
      model: 'user',
      via: 'profile'
    },
    firstName: {
      type: "string"
    },
    lastName: {
      type: "string"
    },
    avatar: {
      type: "string"
    }
  },
  beforeCreate: function (values, callback) {
    if (typeof(values.avatar) == 'undefined') {
      values.avatar = "/images/default-avatar.png";
    }
    callback();
  }
};
