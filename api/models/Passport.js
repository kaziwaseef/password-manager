/**
 * Passport.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

var bcrypt = require('bcrypt');

var supportedLogins = [
  'passport-local', // username && password
  'passport-google-oauth20', // google+ || gmail
  'passport-facebook' // facebook
];

module.exports = {

		attributes: {
			user: {
				model: 'user',
				via: 'passport'
			},
			loginType: {
				type: 'string',
				enum: supportedLogins
			},
			email: {
				type: 'email',
				unique: true
			},
			// For passport-local
			password: {
				type: 'string'
			},
			// For passport-google-oauth20 passport-facebook
			oAuthUniqueID: {
				type: 'string',
				unique: true
			},
			// For encryption
			masterPassword: {
				type: 'string',
				defaultsTo: null
			},
      passwordResetRequest: {
        type: 'string',
        defaultsTo: null
      },
			toJSON: function () {
				var obj = this.toObject();
				delete obj.password;
				delete obj.oAuthUniqueID;
				delete obj.masterPassword;
				return obj;
			}
		},
		beforeCreate: function (passport, cb) {
			if (passport.password == undefined) {
				cb();
			} else {
				CryptoService.getHashSaltLength(function (err, saltLength) {
						bcrypt.genSalt(saltLength, function (err, salt) {
							bcrypt.hash(passport.password, salt, function (err, hash) {
								if (err) {
									cb(err);
								} else {
									passport.password = hash;
									cb();
								}
							});
						});
					});
				}
			},
			beforeUpdate: function (passport, cb) {
				if (passport.masterPassword == undefined || passport.masterPassword == null) {
					cb();
				} else {
					CryptoService.getMasterSaltLength(function (err, saltLength) {
						bcrypt.genSalt(saltLength, function (err, salt) {
							bcrypt.hash(passport.masterPassword, salt, function (err, hash) {
								if (err) {
									cb(err);
								} else {
									passport.masterPassword = hash;
									cb();
								}
							});
						});
					});
				}
			}
		};
