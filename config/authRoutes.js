/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#!/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

  'get /login': {
		view: 'auth/login'
	},
	'post /login': 'AuthController.login',

	'get /login/google': 'AuthController.google',
	'get /auth/google/callback': {
    policy: 'googleOAuth',
    controller: 'AuthController',
    action: 'googleCallback'
	},

	'get /login/facebook': 'AuthController.facebook',
	'get /auth/facebook/callback': {
    policy: 'facebookOAuth',
    controller: 'AuthController',
    action: 'facebookCallback'
	},

  'get /signup': {
		view: 'auth/signup'
	},
	'post /signup': 'AuthController.signup',

  'get /reset-password': {
		view: 'auth/reset'
	},

	'post /reset-password': 'AuthController.resetPassword',

  'get /recover-password': 'AuthController.recoverPassword',

  'post /recover-password': 'AuthController.recoverPasswordProcessing',

	'/logout': 'AuthController.logout'

};
